package com.javacodegeeks.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class ApplicationController {

	@RequestMapping(value = "/Test", method = RequestMethod.GET)
	public String welcome(ModelMap model) throws NamingException, SQLException {
		// model.addAttribute("msgArgument",
		// "Maven Java Web Application Project: Success!");

		/**
		 * Get initial context that has references to all configurations and
		 * resources defined for this web application.
		 */
		Context initialContext = new InitialContext();

		/**
		 * Get Context object for all environment naming (JNDI), such as
		 * Resources configured for this web application.
		 */
		Context environmentContext = (Context) initialContext
				.lookup("java:comp/env");
		/**
		 * Name of the Resource we want to access.
		 */
		String dataResourceName = "jdbc/JCGExampleDB";
		/**
		 * Get the data source for the MySQL to request a connection.
		 */
		DataSource dataSource = (DataSource) environmentContext
				.lookup(dataResourceName);
		/**
		 * Request a Connection from the pool of connection threads.
		 */
		Connection conn = dataSource.getConnection();
		StringBuilder msg = new StringBuilder();
		/**
		 * Use Connection to query the database for a simple table listing.
		 * Statement will be closed automatically.
		 */
		try (Statement stm = conn.createStatement()) {
			String query = "show tables;";
			ResultSet rs = stm.executeQuery(query);
			// Store and return result of the query
			while (rs.next()) {
				msg.append(rs.getString("Tables_in_JCGExampleDB"));
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			// Release connection back to the pool
			if (conn != null) {
				conn.close();
			}
			conn = null; // prevent any future access
		}

		model.addAttribute("msgArgument",
				"Maven Java Web Application Project: Success! The show tables result is: "
						+ msg.toString());

		return "index";
	}

	@RequestMapping(value = "/Print/{arg}", method = RequestMethod.GET)
	public String welcomeName(@PathVariable String arg, ModelMap model) {
		model.addAttribute("msgArgument",
				"Maven Java Web Application Project, input variable: " + arg);

		return "index";
	}
}